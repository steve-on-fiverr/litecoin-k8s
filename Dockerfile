FROM debian:stable-slim

ARG GF_UID="200"
ARG GF_GID="200"

RUN groupadd -r -g $GF_GID litecoin \
  && useradd -r -u $GF_UID -g litecoin litecoin \
  && apt-get update -y \
  && apt-get install -y curl gnupg \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV LITECOIN_VERSION=0.18.1
ENV LITECOIN_DATA=/home/litecoin/.litecoin

RUN curl -SLO https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz \
  && echo "ca50936299e2c5a66b954c266dcaaeef9e91b2f5307069b9894048acf3eb5751 litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz" | sha256sum -c \
  && tar --strip=2 -xzf *.tar.gz -C /usr/local/bin \
  && rm *.tar.gz

VOLUME ["/home/litecoin/.litecoin"]

EXPOSE 9332
USER litecoin

ENTRYPOINT ["litecoind"]
CMD ["-regtest=1", "--printtoconsole"]
